<?php

namespace SolicitacoesCoc;

use Illuminate\Database\Eloquent\Model;

class Gestores extends Model
{
    protected $fillable = [
        're',
        'nome',
        'telefone',
        'email'
    ];
}
