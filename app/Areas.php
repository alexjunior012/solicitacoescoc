<?php

namespace SolicitacoesCoc;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected $fillable = [
        'descricao',
        'codArea'
    ];
}
