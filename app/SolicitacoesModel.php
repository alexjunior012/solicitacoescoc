<?php

namespace SolicitacoesCoc;

use Illuminate\Database\Eloquent\Model;

class SolicitacoesModel extends Model
{
    protected $fillable = [
        'solicitante',
        're',
        'area',
        'telefone',
        'email',
        'gestor',
        'data_cadastramento',
        'objetivo',
        'solicitacao',
        'prazo_inicial',
        'prazo_final',
        'observacao',
        'status',
        'iniciado_em',
        'finalizado_em',
        'tempo_execucao',
        'dias_atraso',
        'responsavel_tropa',
        're_tropa',
        'telefone_tropa',
        'email_tropa'
    ];
}
