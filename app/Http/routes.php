<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

//CRUD de Areas
Route::group(['prefix' => 'areas', 'where'=>['idArea'=>'[0-9]+']], function() {
    Route::get('', ['as' => 'areas.index', 'uses' => 'areasController@index']);
    Route::get('create', ['as'=>'areas.create', 'uses'=>'areasController@create']);
    Route::post('save', ['as'=>'areas.save', 'uses'=>'areasController@save']);
    Route::get('{idArea}/edit', ['as'=>'areas.edit', 'uses' => 'areasController@edit']);
    Route::put('{idArea}/update', ['as'=>'areas.update', 'uses' => 'areasController@update']);
    Route::get('{idArea}/delete', ['as'=>'areas.delete', 'uses'=>'areasController@delete']);
});

//CRUD de Gestores
Route::group(['prefix' => 'gestores'], function() {
    Route::get('', ['as'=>'gestores.index', 'uses'=>'GestoresController@index']);
    Route::get('create', ['as'=>'gestores.create', 'uses'=>'GestoresController@create']);
    Route::post('save', ['as'=>'gestores.save', 'uses'=>'GestoresController@save']);
    Route::get('{idGestor}/edit', ['as'=>'gestores.edit', 'uses'=>'GestoresController@edit']);
    Route::put('{idGestor}/update', ['as'=>'gestores.update', 'uses'=>'GestoresController@update']);
    Route::get('{idGestor}/delete', ['as'=>'gestores.delete', 'uses'=>'GestoresController@delete']);
});

//CRUD de Solicitações
Route::group(['prefix' => 'solicitacoes', 'where'=>['idSolicitacao'=>'[0-9]+']], function(){
    Route::get('', ['as'=>'solicitacoes.index', 'uses' => 'SolicitacoesCocController@index']);
    Route::get('create', ['as'=>'solicitacoes.create', 'uses' => 'SolicitacoesCocController@create']);
    Route::post('save', ['as'=>'solicitacoes.save', 'uses' => 'SolicitacoesCocController@save']);
    Route::get('{idSolicitacao}/edit', ['as'=>'solicitacoes.edit', 'uses' => 'SolicitacoesCocController@edit']);
    Route::put('{idSolicitacao}/update', ['as'=>'solicitacoes.update', 'uses' => 'SolicitacoesCocController@update']);
    Route::get('{idSolicitacao}/delete', ['as'=>'solicitacoes.delete', 'uses' => 'SolicitacoesCocController@delete']);
});

//CRUD de Usuarios
Route::group(['prefix'=>'usuarios'], function(){
    Route::get('', ['as'=>'usuarios.index', 'uses'=>'UsuariosController@index']);
    Route::get('create', ['as'=>'usuarios.create', 'uses'=>'UsuariosController@create']);
});


//Route::get('/teste', 'SolicitacoesCocController@teste');











