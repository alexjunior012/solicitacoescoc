<?php

namespace SolicitacoesCoc\Http\Requests;

use SolicitacoesCoc\Http\Requests\Request;

class GestoresRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            're'  => 'required',
            'nome'  => 'required',
            'telefone'  => 'required',
            'email'  => 'required'
        ];
    }
}
