<?php

namespace SolicitacoesCoc\Http\Requests;

use SolicitacoesCoc\Http\Requests\Request;

class SolicitacoesCocRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            're' => 'required',
//            'solicitante' => 'required',
//            'area' => '',
//            'telefone' => 'required',
//            'email' => '',
//            'gestor' => '',
//            'data_cadastramento' => '',
//            'objetivo' => '',
//            'solicitacao' => '',
//            'prazo_inicial' => '',
//            'prazo_final' => '',
//            'observacao' => '',
//            'status' => '',
//            'iniciado_em' => '',
//            'finalizado_em' => '',
//            'tempo_execucao' => '',
//            'dias_atraso' => '',
//            'responsavel_tropa' => '',
//            're_tropa' => '',
//            'telefone_tropa' => '',
//            'email_tropa' => '',
        ];
    }
}
