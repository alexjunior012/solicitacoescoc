<?php
/**
 * Created by PhpStorm.
 * User: Keyrus
 * Date: 01/04/2016
 * Time: 15:21
 */

namespace SolicitacoesCoc\Http\Controllers;

use SolicitacoesCoc\Http\Controllers\areasController,
    SolicitacoesCoc\Http\Controllers\SolicitacoesCocController;


class HomeController extends Controller
{
    public function index()
    {
        $arraySolicitacoes = $this->getTop10Pendentes();

        return view('layout.home', ['solicitacoes'=>$arraySolicitacoes]);
    }

    public function getTop10Pendentes()
    {
        $oSolicitacao = new SolicitacoesCocController();
        $arraySolicitacao = $oSolicitacao->getTop10Pendentes();


        return $arraySolicitacao;
    }


}