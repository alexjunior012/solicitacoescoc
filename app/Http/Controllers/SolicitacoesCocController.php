<?php
/**
 * Created by PhpStorm.
 * User: Keyrus
 * Date: 31/03/2016
 * Time: 12:47
 */

namespace SolicitacoesCoc\Http\Controllers;

use SolicitacoesCoc\SolicitacoesModel;
use SolicitacoesCoc\Http\Requests;
use SolicitacoesCoc\Http\Requests\SolicitacoesCocRequest;


class SolicitacoesCocController extends Controller
{

    private function getAll()
    {
        $arraySolicitacoes = SolicitacoesModel::all();
        return $arraySolicitacoes;
    }

    public function getTop10Pendentes()
    {
        $result = \DB::table('solicitacoes_models')->where('status', '=', 'Pendente');
        return $result;
    }

    public function index()
    {
        return view('solicitacoes.index', ['solicitacoes' => $this->getAll()]);
    }

    public function create()
    {
        return view('solicitacoes.create');
    }

    public function save(SolicitacoesCocRequest $request)
    {
        $input = $request->all();
        SolicitacoesModel::create($input);
        return redirect()->route('solicitacoes.index');
    }

    public function edit($idSolicitacao)
    {
        $solicitacao = SolicitacoesModel::find($idSolicitacao);
        return view('solicitacoes.edit', compact('solicitacao'));
    }

    public function update(SolicitacoesCocRequest $request, $idSolicitacao)
    {
        $solicitacao = SolicitacoesModel::find($idSolicitacao)->update($request->all());
        return redirect()->route('solicitacoes.index');
    }

    public function delete($id)
    {
        SolicitacoesModel::find($id)->delete();
        return redirect()->route('solicitacoes.index');
    }



}