<?php
/**
 * Created by PhpStorm.
 * User: Keyrus
 * Date: 12/04/2016
 * Time: 15:35
 */

namespace SolicitacoesCoc\Http\Controllers;

use SolicitacoesCoc\Http\Requests;
use SolicitacoesCoc\UsuariosModel;
use SolicitacoesCoc\Http\Requests\UsuariosRequest;

class UsuariosController extends Controller
{
    public function index()
    {
        return view('usuarios.index');
    }

    public function create()
    {
        return view('usuarios.create');
    }
}