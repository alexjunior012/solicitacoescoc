<?php
/**
 * Created by PhpStorm.
 * User: Keyrus
 * Date: 01/04/2016
 * Time: 16:54
 */

namespace SolicitacoesCoc\Http\Controllers;

use SolicitacoesCoc\Areas;
use SolicitacoesCoc\Http\Requests;
use SolicitacoesCoc\Http\Requests\AreasRequest;

class AreasController extends Controller
{
    public function getAll()
    {
        $areas = Areas::all();
        if(count($areas) == 0)
            return false;

        return $areas;
    }

    public function index()
    {
        return view('areas.index', ['areas' => $this->getAll()]);
    }

    public function create()
    {
        return view('areas.create');
    }

    public function save(AreasRequest $request)
    {
        $inputs = $request->all();
        Areas::create($inputs);
        return redirect()->route('areas.index');
    }

    public function edit($idArea)
    {
        $area = Areas::find($idArea);
        return view('areas.edit', compact('area'));
    }

    public function update(AreasRequest $request, $id)
    {
        $gestor = Areas::find($id)->update($request->all());
        return redirect()->route('areas.index');
    }

    public function delete($id)
    {
//        Areas::find($id)->delete();
//        return redirect()->route('areas.index');

        Areas::find($id)->delete();
        $arrayRetorno = [
            'mensagem' => 'Deletado com sucesso!',
            'class' => 'alert-success',
        ];
        return $arrayRetorno;
    }


}