<?php
/**
 * Created by PhpStorm.
 * User: Keyrus
 * Date: 19/04/2016
 * Time: 17:54
 */

namespace SolicitacoesCoc\Http\Controllers;

use SolicitacoesCoc\Gestores;
use SolicitacoesCoc\Http\Requests;
use SolicitacoesCoc\Http\Requests\GestoresRequest;

class GestoresController extends Controller
{
    private function getAll()
    {
        $gestores = Gestores::all();
        if(count($gestores) == 0)
            return false;

        return $gestores;
    }

    public function index()
    {
        return view('gestores.index', ['gestores' => $this->getAll()]);
    }

    public function create()
    {
        return view('gestores.create');
    }

    public function save(GestoresRequest $request)
    {
        $inputs = $request->all();
        Gestores::create($inputs);

        return redirect()->route('gestores.index');
    }

    public function edit($id)
    {
        $gestor = Gestores::find($id);
        return view('gestores.edit', compact('gestor'));
    }

    public function update(GestoresRequest $request, $id)
    {
        $gestor = Gestores::find($id)->update($request->all());
        return redirect()->route('gestores.index');
    }

    public function delete($id)
    {
        Gestores::find($id)->delete();
        return redirect()->route('gestores.index');
    }

}