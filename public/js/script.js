/**
 * Created by Keyrus on 01/04/2016.
 */


function exibeModalDelete(area, id){
    $('#deletarArea .modal-body *').remove();
    $('#deletarArea .modal-body').append('<p>Deseja realmente deletar a area <b>'+area+'</b></p>');
    $('#deletarArea').modal('show');

    $('#btnConfirmaDeletar').click(function(){
        deletaArea(id);
    });
}

function deletaArea(id){

    console.log('este e a area: '+id);
    //filterTipoContato = typeof filterTipoContato !== 'undefined' ? filterTipoContato : '0';
    //filterCanal = typeof filterCanal   !== 'undefined' ? filterCanal   : '0';
    //periodo = typeof periodo !== 'undefined' ? periodo : 'D'; // 'D'iario
    //
    //var url = "/" + $('meta[name="env"]').attr('content') + urlAJax;

    $.ajax({
        url: '/areas/'+id+'/delete',
        dataType: 'json',
        type: "GET",
        cache: false,
        data: {
            id: id
        },
        quietMillis: 250
    }).done(function(data){

        $('#deletarArea').modal('hide');
        $('#mensagensAlerta').addClass(data.class).html(data.mensagem);

    }).fail(function(){
        $(div).html(htmlError);
        return false
    });
}

function listarAreas()
{
    $.ajax({
        url: '/areas/'+id+'/delete',
        dataType: 'json',
        type: "GET",
        cache: false,
        data: {
            id: id
        },
        quietMillis: 250
    }).done(function(data){

        $('#deletarArea').modal('hide');
        $('#mensagensAlerta').addClass(data.class).html(data.mensagem);

    }).fail(function(){
        $(div).html(htmlError);
        return false
    });
}

$(function(){
    $('#acionarMenu').click(function(){
        $('.containerMenuLateral').addClass('is-visible');
        $('.mdl-layout__obfuscator').addClass('is-visible');
    });

    $('.mdl-layout__obfuscator').click(function(){
        $('.containerMenuLateral').removeClass('is-visible');
        $('.mdl-layout__obfuscator').removeClass('is-visible');
    });

    $('.tabelaAreas .btnDeleteArea').click(function(){
        exibeModalDelete( $(this).parent().parent().attr('id'), $(this).parent().attr('id') );
    });

    //$('#graficoGanttTropa').highcharts({
    //
    //    chart: {
    //        type: 'columnrange',
    //        inverted: true
    //    },
    //    title: {
    //        text: 'Temperature variation by month'
    //    },
    //    subtitle: {
    //        text: 'Observed in Vik i Sogn, Norway'
    //    },
    //    xAxis: {
    //        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    //    },
    //    yAxis: {
    //        title: {
    //            text: 'Temperature ( °C )'
    //        }
    //    },
    //    tooltip: {
    //        valueSuffix: '°C'
    //    },
    //    plotOptions: {
    //        columnrange: {
    //            dataLabels: {
    //                enabled: true,
    //                formatter: function () {
    //                    return this.y + '°C';
    //                }
    //            }
    //        }
    //    },
    //    legend: {
    //        enabled: false
    //    },
    //    series: [{
    //        name: 'Temperatures',
    //        data: [
    //            [-9.7, 9.4],
    //            [-8.7, 6.5],
    //            [-3.5, 9.4],
    //            [-1.4, 19.9],
    //            [0.0, 22.6],
    //            [2.9, 29.5],
    //            [9.2, 30.7],
    //            [7.3, 26.5],
    //            [4.4, 18.0],
    //            [-3.1, 11.4],
    //            [-5.2, 10.4],
    //            [-13.5, 9.8]
    //        ]
    //    }]
    //
    //});


    $('#solicitacoesFinalizadas').highcharts({
        colors: [
            '#5DD379',
            '#D3554E',
        ],
        chart: {
            type: 'column'
            //inverted: true
        },
        title: {
            text: 'Solicitações Finalizadas'
        },
        xAxis: {
            categories: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: 'Executadas sem atraso',
            data: [5, 3, 4, 7, 2, 2, 2, 3, 2, 1, 2, 4]
        }, {
            name: 'Executadas com atraso',
            data: [2, 2, 3, 2, 1, 5, 3, 4, 7, 2, 2, 4]
        }]
    });

    $('#solicitacoesAnuais').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Solicitações anuais'
        },
        subtitle: {
            //text: 'Source: WorldClimate.com'
        },
        xAxis: {
            categories: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: '2016',
            data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            name: '2015',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }, {
            name: '2014',
            data: [5.9, 2.2, 3.7, 6.5, 9.9, 1.2, 7.0, 6.6, 19.2, 12.3, 1.6, 5.8]
        }]
    });


    $('#responsaveisSolicitacao').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Responsaveis'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
            {
                name: 'Sem Responsavel',
                y: 20.00,
                sliced: true,
                selected: true
            }, {
                name: 'Itamar',
                y: 10.00
            }, {
                name: 'Marco',
                y: 10.00
            }, {
                name: 'Roberson',
                y: 10.00
            }, {
                name: 'Thiago Furtado',
                y: 10.00
            }, {
                name: 'Rodrigo',
                y: 10.00
            }, {
                name: 'Thiago Gonçalves',
                y: 10.00
            }, {
                name: 'Luiz',
                y: 10.00
            }, {
                name: 'Alex',
                y: 10.00
            }]
        }]
    });




});