<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasGestoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_gestores', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_area')->unsigned();
            $table->foreign('id_area')->references('id')->on('areas')->onDelete('cascade');

            $table->integer('id_gestor')->unsigned();
            $table->foreign('id_gestor')->references('id')->on('gestores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('areas_gestores');
    }
}
