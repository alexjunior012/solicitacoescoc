<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sistema de controle de solicitações para a equipe tropa de elite">
        <meta name="author" content="Alex junior">
        <title>Painel Tropa</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}">
        <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/ie10-viewport-bug-workaround.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/non-responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
    </head>

    <body>
        <nav class="navbar navbar-default navbar-fixed-top menuPreto">
            <div class="row">
                <div class="col-md-2">
                    <div id="acionarMenu">
                        <a href="javascript:void(0)">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <a id="NomeTropa" class="navbar-brand" href="{{ url('/') }}">Tropa de Elite - Painel</a>
                </div>
                <div class="col-md-7">
                    <div id="navbar">
                        <div class="nav">
                            <div class="col-lg-12" id="containerBusca">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Buscar ..." id="campoBusca">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="containerMenuLateral">
            <nav class="menuLateral">
                <a class="linkMenuLateral" href=""></a>
                <a class="linkMenuLateral" href="{{ route('home') }}">Home</a>
                <a class="linkMenuLateral" href="{{ route('solicitacoes.index') }}">Solicitações</a>
                <a class="linkMenuLateral" href="{{ route('areas.index') }}">Areas</a>
                <a class="linkMenuLateral" href="{{ route('gestores.index') }}">Gestores</a>
                <a class="linkMenuLateral" href="{{ route('areas.index') }}">Usuarios</a>
            </nav>
        </div>

        <div class="container fundoCinza">
            @yield('content')
        </div>

        <div class="mdl-layout__obfuscator"></div>
        <br>

        <footer class="fixed-bottom">
            <ul>
                <li>Contato</li>
                <li>Mapa do site</li>
                <li>assas</li>
                <li>assas</li>
            </ul>
        </footer>

        <script src="{{ asset('/js/jquery-1.12.2.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/script.js') }}"></script>
        <script src="{{ asset('/js/highcharts/highcharts.js') }}"></script>

    </body>
</html>
