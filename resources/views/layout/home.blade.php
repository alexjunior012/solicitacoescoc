@extends('layout/index')

@section('content')
    <header role="heading">
        <h2>Titulo aqui</h2>
    </header>

    <div class="row">
        <div class="col-md-12">
            <div class="powerwidget powerwidget-sortable" data-widget-editbutton="false" role="widget">
                <div class="inner-spacer" role="content" style="padding: 0">
                    <div id="graficoGanttTropa" style="height: 350px">
                        <div class="preLoader">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span>Carregando...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row" class="powerwidget">
        <div class="col-md-6">
            <div class="powerwidget powerwidget-sortable" data-widget-editbutton="false" role="widget">
                <header role="heading">
                    <h2>Sem responsavel</h2>
                </header>
                <div class="inner-spacer" role="content" style="padding: 0">
                    <div id="top5pendentes">
                        {{--<div class="preLoader">--}}
                            {{--<div class="progress progress-striped active">--}}
                                {{--<div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">--}}
                                    {{--<span>Carregando...</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="">Código</th>
                                    <th class="">Objetivo</th>
                                    <th class="">Solicitante</th>
                                    <th class="">Data Conclusao</th>
                                    <th class=""></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="">1</td>
                                    <td class="">teste</td>
                                    <td class="">teste</td>
                                    <td class="">12/06/2016</td>
                                    <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="">2</td>
                                    <td class="">teste</td>
                                    <td class="">teste</td>
                                    <td class="">12/06/2016</td>
                                    <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="">3</td>
                                    <td class="">teste</td>
                                    <td class="">teste</td>
                                    <td class="">12/06/2016</td>
                                    <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="">4</td>
                                    <td class="">teste</td>
                                    <td class="">teste</td>
                                    <td class="">12/06/2016</td>
                                    <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="">5</td>
                                    <td class="">teste</td>
                                    <td class="">teste</td>
                                    <td class="">12/06/2016</td>
                                    <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <ul class="pagination">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-6">
            <div class="powerwidget powerwidget-sortable" data-widget-editbutton="false" role="widget">
                <header role="heading">
                    <h2>10 ultimas finalizadas</h2>
                </header>
                <div class="inner-spacer" role="content" style="padding: 0">
                    <div id="top5pendentes">
                        {{--<div class="preLoader">--}}
                        {{--<div class="progress progress-striped active">--}}
                        {{--<div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">--}}
                        {{--<span>Carregando...</span>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="">Código</th>
                                <th class="">Objetivo</th>
                                <th class="">Solicitante</th>
                                <th class="">Data Cadastro</th>
                                <th class=""></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="">1</td>
                                <td class="">teste</td>
                                <td class="">teste</td>
                                <td class="">12/06/2016</td>
                                <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                            <tr>
                                <td class="">2</td>
                                <td class="">teste</td>
                                <td class="">teste</td>
                                <td class="">12/06/2016</td>
                                <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                            <tr>
                                <td class="">3</td>
                                <td class="">teste</td>
                                <td class="">teste</td>
                                <td class="">12/06/2016</td>
                                <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                            <tr>
                                <td class="">4</td>
                                <td class="">teste</td>
                                <td class="">teste</td>
                                <td class="">12/06/2016</td>
                                <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                            <tr>
                                <td class="">5</td>
                                <td class="">teste</td>
                                <td class="">teste</td>
                                <td class="">12/06/2016</td>
                                <td class=""><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <ul class="pagination">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="powerwidget powerwidget-sortable" data-widget-editbutton="false" role="widget">
                <div class="inner-spacer" role="content" style="padding: 0">
                    <div id="solicitacoesFinalizadas" style="height: 350px">
                        <div class="preLoader">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span>Carregando...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        {{--<header role="heading">--}}
            {{--<h2>Titulo qualquer aqui</h2>--}}
        {{--</header>--}}
        <div class="col-md-6">
            <div class="powerwidget powerwidget-sortable" data-widget-editbutton="false" role="widget">
                <div class="inner-spacer" role="content" style="padding: 0">
                    <div id="solicitacoesAnuais">
                        <div class="preLoader">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span>Carregando...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="powerwidget powerwidget-sortable" data-widget-editbutton="false" role="widget">
                <div class="inner-spacer" role="content" style="padding: 0">
                    <div id="responsaveisSolicitacao">
                        <div class="preLoader">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span>Carregando...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection