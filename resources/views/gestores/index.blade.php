@extends('../layout/index')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($gestores == false)
                <div class="jumbotron alert-warning">
                    <h1>Listagem de Gestores</h1><br>
                    <p>Não foram encontrada gestores registrados</p>
                    <p>Cadastre uma novo gestor no sistema</p>
                </div>
            @else
                <h3>Listagem de Gestores</h3>
                <br>

                <div class="alert" id="mensagensAlerta"></div>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="">re</th>
                        <th class="">nome</th>
                        <th class="">telefone</th>
                        <th class="">email</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($gestores as $gestor)
                        <tr>
                            <td class="">{{ $gestor->re }}</td>
                            <td class="">{{ $gestor->nome }}</td>
                            <td class="">{{ $gestor->telefone }}</td>
                            <td class="">{{ $gestor->email }}</td>
                            <td><a href="{{ route('gestores.edit', ['idGestor'=>$gestor->id ]) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                            <td><a href="{{ route('gestores.delete', ['idGestor'=>$gestor->id ]) }}" class=""><i class="glyphicon glyphicon-trash"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('gestores.create') }}" class="col-md-12 btn btn-primary ">Novo Gestor</a>
        </div>
    </div>
@endsection