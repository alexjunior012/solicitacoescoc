@extends('layout/index')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Editar de Gestor: {{ $gestor->nome }}</h3>
        </div>
    </div>
    <br>

    @if ($errors->any())
        <div class="row">
            <div class="col-md-12">
                <ul class="alert alert-warning">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    {!! Form::open(['url'=>"gestores/$gestor->id/update", "method"=>'put']) !!}
        <div class="row">
            <div class="col-md-2">
                {!! Form::label('re', 'RE:') !!}
                {!! Form::text('re', $gestor->re, ['class'=>'form-control', 'placeholder'=>'Ex: CFF', 'id'=>'re']) !!}
            </div>

            <div class="col-md-10">
                {!! Form::label('nome', 'Nome:') !!}
                {!! Form::text('nome', $gestor->nome, ['class'=>'form-control', 'placeholder'=>'Informa seu nome', 'id'=>'nome']) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-4">
                {!! Form::label('telefone', 'Telefone:') !!}
                {!! Form::text('telefone', $gestor->telefone, ['class'=>'form-control', 'placeholder'=>'Ex: 11 xxxx-xxxx', 'id'=>'telefone']) !!}
            </div>
            <div class="col-md-8">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', $gestor->email, ['class'=>'form-control', 'placeholder'=>'Ex: seuemail@dominio.com', 'id'=>'email']) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-danger" href="{{ route('gestores.index') }}">Cancelar</a>
            </div>
            <div class="col-md-6">
                {!! Form::submit('Atualizar', ['class'=>'btn btn-success']) !!}
            </div>
        </div>
    {!! Form::close() !!}
@endsection