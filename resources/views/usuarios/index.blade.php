@extends('../layout/index')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Listagem de Usuarios</h3>
            <br>

            <table class="table table-hover">
                <thead>
                    <tr>
                        {{--<th class="">Código</th>--}}
                        {{--<th class="">Objetivo</th>--}}
                        {{--<th class="">Solicitante</th>--}}
                        {{--<th class="">Data Cadastro</th>--}}
                        {{--<th class="">Status</th>--}}
                        {{--<th></th>--}}
                        {{--<th></th>--}}
                    </tr>
                </thead>
                <tbody>
                {{--@foreach($solicitacoes as $solicitacao)--}}
                    {{--<tr>--}}
                        {{--<td class="">{{ $solicitacao->id }}</td>--}}
                        {{--<td class="">{{ $solicitacao->objetivo }}</td>--}}
                        {{--<td class="">{{ $solicitacao->solicitante }}</td>--}}
                        {{--<td class="">{{ $solicitacao->data_cadastramento }}</td>--}}
                        {{--<td class="">{{ $solicitacao->status }}</td>--}}
                        {{--<td><a href="{{ route('solicitacoes.edit', ['idSolicitacao'=>$solicitacao->id ]) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>--}}
                        {{--<td><a href="{{ route('solicitacoes.delete', ['idSolicitacao'=>$solicitacao->id ]) }}"><i class="glyphicon glyphicon-trash"></i></a></td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                </tbody>
            </table>

            <a href="{{ route('usuarios.create') }}" class="btn btn-primary">Novo usuário</a>
        </div>
    </div>

@endsection