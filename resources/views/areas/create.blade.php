@extends('layout/index')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Cadastro de Areas</h3>
        </div>
    </div>
    <br>

    @if ($errors->any())
        <div class="row">
            <div class="col-md-12">
                <ul class="alert alert-warning">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    {!! Form::open(['url'=>'areas/save']) !!}
        <div class="row">
            <div class="col-md-6">
                {!! Form::label('codArea', 'Cod. Area:') !!}
                {!! Form::text('codArea', '', ['class'=>'form-control', 'placeholder'=>'Ex: CFF', 'id'=>'codArea']) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-12">
                {!! Form::label('descricao', 'Descrição Area:') !!}
                {!! Form::textarea('descricao', '', ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>'Qual a descrição da area', 'id'=>'descricao', 'id'=>"descricao"]) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-danger" href="{{ route('areas.index') }}">Cancelar</a>
            </div>

            <div class="col-md-6">
                {!! Form::submit('Cadastrar', ['class'=>'btn btn-success']) !!}
            </div>
        </div>
    {!! Form::close() !!}
@endsection