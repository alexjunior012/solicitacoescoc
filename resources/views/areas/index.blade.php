@extends('../layout/index')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($areas == false)
                <div class="jumbotron alert-warning">
                    <h1>Listagem de areas</h1><br>
                    <p>Não foram encontrada areas registradas</p>
                    <p>Cadastre uma nova area no sistema</p>
                </div>
            @else
                <h3>Listagem de Areas</h3>
                <br>

                <div class="alert" id="mensagensAlerta"></div>

                <table class="table table-hover tabelaAreas">
                    <thead>
                    <tr>
                        <th class="">Código</th>
                        <th class="">Descrição</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($areas as $area)
                        <tr id="{{$area->codArea}}">
                            <td class="">{{ $area->codArea }}</td>
                            <td class="">{{ $area->descricao }}</td>
                            <td><a href="{{ route('areas.edit', ['idSolicitacao'=>$area->id ]) }}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                            {{--<td><a href="{{ route('areas.delete', ['idSolicitacao'=>$area->id ]) }}" class="btnDeleteArea"><i class="glyphicon glyphicon-trash"></i></a></td>--}}
                            <td id="{{ $area->id }}"><a class="btnDeleteArea"><i class="glyphicon glyphicon-trash"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('areas.create') }}" class="col-md-12 btn btn-primary ">Nova Area</a>
        </div>
    </div>

    <div class="modal" id="deletarArea">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body text-center"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="trigger-deletewidget-reset">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="btnConfirmaDeletar">Deletar</button>
                </div>
            </div>
        </div>
    </div>
@endsection