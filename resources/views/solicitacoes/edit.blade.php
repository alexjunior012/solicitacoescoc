@extends('../layout/index')

@section('content')
    <h3>Editar Solicitação: {{ $solicitacao->id }}</h3><br>

    @if ($errors->any())
        <ul class="alert alert-warning">
            @foreach($errors->all() as $error)
                 <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {{-- solicitacoes/save --}}
    {!! Form::open(['url'=>"solicitacoes/$solicitacao->id/update", 'method'=>'put']) !!}
    {{--{!! Form::open() !!}--}}
        <div class="row">
            <div class="col-md-2">
                {!! Form::label('re', 'RE:') !!}
                {!! Form::text('re', $solicitacao->re, ['class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Informe seu RE', 'id'=>'re']) !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('solicitante', 'Solicitante:') !!}
                {!! Form::text('solicitante', $solicitacao->solicitante, ['class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Seu nome', 'id'=>'solicitante']) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', $solicitacao->email, ['class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'seuEmail@telefonica.com', 'id'=>'email']) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-6">
                {!! Form::label('objetivo', 'Objetivo:') !!}
                {!! Form::textarea('objetivo', $solicitacao->objetivo, ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>'Informe o objetivo desta solicitação', 'id'=>'objetivo']) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('observacao', 'Observações:') !!}
                {!! Form::textarea('observacao', $solicitacao->observacao, ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>'Deixe sua observação caso ache necessário', 'id'=>'observacao']) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-12">
                {!! Form::label('solicitacao', 'Descrição da Solicitação:') !!}
                {!! Form::textarea('solicitacao', $solicitacao->solicitacao, ['class'=>'form-control', 'rows'=>'10', 'placeholder'=>'Descreva a solicitação', 'id'=>'solicitacao']) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-4">
                {!! Form::label('prazo_inicial', 'Prazo Inicial:') !!}
                {!! Form::text('prazo_inicial', $solicitacao->prazo_inicial, ['class'=>'form-control', 'readonly'=>'readonly', 'id'=>'prazo_inicial']) !!}
            </div>

            <div class="col-md-4">
                {!! Form::label('prazo_final', 'Prazo Final:') !!}
                {!! Form::text('prazo_final', $solicitacao->prazo_final, ['class'=>'form-control', 'readonly'=>'readonly', 'id'=>'prazo_final']) !!}
            </div>

            <div class="col-md-4">
                {!! Form::label('status', 'Status:') !!}
                {!! Form::text('status', $solicitacao->status, ['class'=>'form-control', 'id'=>'status']) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-12">
                <label for="exampleInputName2">Anexos:</label>
                <input type="file" class="form-control" id="exampleInputEmail2" placeholder="Seu nome">
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-danger" href="{{ route('solicitacoes.index') }}">Cancelar</a>
            </div>

            <div class="col-md-6">
                {!! Form::submit('Salvar', ['class'=>'btn btn-success']) !!}
                {{--<a class="btn btn-success" href="{{ route('solicitacoes.update', ['idSolicitacao'=>$solicitacao->id]) }}">Salvar</a>--}}
            </div>
        </div>


    {!! Form::close() !!}

@endsection