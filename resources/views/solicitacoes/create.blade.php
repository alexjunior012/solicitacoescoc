@extends('../layout/index')

@section('content')
    <h3>Cadastro de Solicitação</h3><br>
    <p>Data de cadastramento: <?= date('d/m/Y') ?></p>
    <br>

    @if ($errors->any())
        <ul class="alert alert-warning">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {!! Form::open(['url'=>'solicitacoes/save']) !!}
        {!! Form::hidden('data_cadastramento', date('Y-m-d'), ['class'=>'form-control', 'id'=>'data_cadastramento']) !!}
        {!! Form::hidden('area', 'CFF', ['class'=>'form-control', 'id'=>'area']) !!}
        {!! Form::hidden('telefone', '3549-7108', ['class'=>'form-control', 'id'=>'telefone']) !!}
        {!! Form::hidden('gestor', 'Rafael Sgrott Martins', ['class'=>'form-control', 'id'=>'gestor']) !!}
        {!! Form::hidden('status', 'FINALIZADA NORMAL', ['class'=>'form-control', 'id'=>'status']) !!}
        {!! Form::hidden('iniciado_em', '08/07/2011', ['class'=>'form-control', 'id'=>'iniciado_em']) !!}
        {!! Form::hidden('finalizado_em', '18/04/2011', ['class'=>'form-control', 'id'=>'finalizado_em']) !!}
        {!! Form::hidden('tempo_execucao', '0', ['class'=>'form-control', 'id'=>'tempo_execucao']) !!}
        {!! Form::hidden('dias_atraso', '0', ['class'=>'form-control', 'id'=>'dias_atraso']) !!}
        {!! Form::hidden('responsavel_tropa', 'ITAMAR DE MORAES SILVA', ['class'=>'form-control', 'id'=>'responsavel_tropa']) !!}
        {!! Form::hidden('re_tropa', '107954', ['class'=>'form-control', 'id'=>'re_tropa']) !!}
        {!! Form::hidden('telefone_tropa', '3549-7108', ['class'=>'form-control', 'id'=>'telefone_tropa']) !!}
        {!! Form::hidden('email_tropa', 'itamar.silva@telefonica.com', ['class'=>'form-control', 'id'=>'email_tropa']) !!}

        <div class="row">
            <div class="col-md-2">
                {!! Form::label('re', 'RE:') !!}
                {!! Form::text('re', '', ['class'=>'form-control', 'placeholder'=>'Informe seu RE', 'id'=>'re']) !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('solicitante', 'Solicitante:') !!}
                {!! Form::text('solicitante', '', ['class'=>'form-control', 'placeholder'=>'Seu nome', 'id'=>'solicitante']) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', '', ['class'=>'form-control', 'placeholder'=>'seuEmail@telefonica.com', 'id'=>'email']) !!}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-6">
                {!! Form::label('objetivo', 'Objetivo:') !!}
                {!! Form::textarea('objetivo', '', ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>'Informe o objetivo desta solicitação', 'id'=>'objetivo']) !!}
                <span></span>
            </div>

            <div class="col-md-6">
                {!! Form::label('observacao', 'Observações:') !!}
                {!! Form::textarea('observacao', '', ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>'Deixe sua observação caso ache necessário', 'id'=>'observacao']) !!}
                <span></span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-12">
                {!! Form::label('solicitacao', 'Descrição da Solicitação:') !!}
                {!! Form::textarea('solicitacao', '', ['class'=>'form-control', 'rows'=>'10', 'placeholder'=>'Descreva a solicitação', 'id'=>'solicitacao']) !!}
                <span></span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-4">
                {!! Form::label('prazo_inicial', 'Prazo Inicial:') !!}
                {!! Form::text('prazo_inicial', '', ['class'=>'form-control', 'id'=>'prazo_inicial']) !!}
            </div>

            <div class="col-md-4">
                {!! Form::label('prazo_final', 'Prazo Final:') !!}
                {!! Form::text('prazo_final', '', ['class'=>'form-control', 'id'=>'prazo_final']) !!}
            </div>

            <div class="col-md-4">
                {!! Form::label('anexos', 'Anexos:') !!}
                {!! Form::file('image') !!}
                {{--<label for="exampleInputName2">Anexos:</label>--}}
                {{--<input type="file" class="form-control" readonly id="exampleInputEmail2" placeholder="Seu nome">--}}
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-danger" href="{{ url('/solicitacoes') }}">Cancelar</a>
            </div>

            <div class="col-md-6">
                {!! Form::submit('Salvar', ['class'=>'btn btn-success']) !!}
                {{--<a class="btn btn-success" href="{{ url('/solicitacoes/save') }}">Salvar</a>--}}
            </div>
        </div>
    {!! Form::close() !!}
@endsection